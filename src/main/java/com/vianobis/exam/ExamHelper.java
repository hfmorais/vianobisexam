package com.vianobis.exam;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ExamHelper {

	public static String getPropertyValue(String filepath, String property) {
		Properties properties = new Properties();

		InputStream inputStream = ExamHelper.class.getClassLoader().getResourceAsStream(filepath);
		try {
			if (inputStream != null) {
				properties.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + filepath + "' not found in the classpath");
			}

			String propertyValue = properties.getProperty(property);
			inputStream.close();

			return propertyValue;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// failed to close stream. We'll ignore this error.
			}
		}

		throw new RuntimeException("Failed to load property... Big time!");
	}
}
