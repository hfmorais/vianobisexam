package com.vianobis.exam;

import java.util.List;

import com.vianobis.exam.structure.AbstractStructureNode;

/**
 * For this test we want the implementation of the methods within this class.
 *
 * Overview on how structures work:
 *  - There are 3 types of structure nodes: responsibility, vertical and horizontal.
 *  - They work as a matrix, with a responsibility node at the intersection of a horizontal and a vertical node.
 *  - All nodes can have a parent. If they have a parent, then it will be of the same type (eg: a vertical's parent
 *    will also be a vertical node and so on). There is no limit on the number of "generations".
 *  - If a responsiblity node has a parent, it must not have a horizontal nor a vertical node. It is supposed to
 *    use/inherit the horizontal and vertical from its parent.
 *  - All nodes can have roles. Additionally, besides the roles that each role "owns", child nodes inherit the roles
 *    of their parent. On the other hand, nodes which have no parent must have a vertical and horizontal (as described
 *    above) and will inherit the roles of that horizontal and vertical.
 *
 * Examples:
 *     Horizontal H1 has role ONWER and Vertical V2 has role ADMIN.
 *     Vertical V2_A is a son of V2, so it will also have the ADMIN role.
 *     Responsibility R1 has role PARTICIPANT.
 *     R1 has vertical V2_A and horizontal H1, so it will inherit roles OWNER and ADMIN.
 *     R1 has a child R1_B. R1_B cannot have vertical and horizontal nodes because it's a child. R1_B will inherit roles
 *     PARTICIPANT, OWNER and ADMIN.
 */
public class StructureTest {

	/**
	 * Given a structure type and a role, return a list of all nodes that have that role.
	 *
	 * @param structureClazz Structure node (ex: VerticalNode.class)
	 * @param role           Structure role (ex: VerticalNode.OWNER_ROLE)
	 * @return list of all structure nodes that have that role
	 */
	public List<AbstractStructureNode> findStructuresWithRole(Class<?> structureClazz, String role) {
		//TODO: Complete this method
		return null;
	}

	/**
	 * Given a structure node and a role, check if that node has the given role
	 *
	 * @param node Structure node
	 * @param role Structure role (ex: VerticalNode.OWNER_ROLE)
	 */
	public boolean sctructureHasRole(AbstractStructureNode node, String role) {
		//TODO: Complete this method
		return false;
	}

}
