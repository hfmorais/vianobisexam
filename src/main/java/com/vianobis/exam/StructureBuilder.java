package com.vianobis.exam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vianobis.exam.structure.HorizontalNode;
import com.vianobis.exam.structure.ResponsibilityNode;
import com.vianobis.exam.structure.VerticalNode;

public class StructureBuilder {

	private static final String STRUCTURE_FILE = "StructureNodes.properties";

	private static final String VERTICAL_NODES_PROPERTIES = "VERTICAL-LIST";
	private static final String HORIZONTAL_NODES_PROPERTIES = "HORIZONTAL-LIST";
	private static final String RESPONSIBILITY_NODES_PROPERTIES = "RESPONSIBILITY-LIST";
	
	private static final String STRUCTURES_WITH_ROLES = "STRUCTURES-WITH-ROLES";
	private static final String ROLE_SUFFIX = "_ROLES";

	private Map<String, VerticalNode> verticalMap;
	private Map<String, HorizontalNode> horizontalMap;
	private Map<String, ResponsibilityNode> responsibilityMap;
	
	private List<String> structureRoleCodeList;

	private void loadVerticalNodes() {
		String[] verticals = ExamHelper.getPropertyValue(STRUCTURE_FILE, VERTICAL_NODES_PROPERTIES).split(",");

		verticalMap = new HashMap<>();
		for (String nodeCode : verticals) {
			VerticalNode verticalNode = new VerticalNode();
			verticalNode.setCode(nodeCode);
			verticalNode.setTitle("Vertical " + nodeCode);

			// Load roles
			List<String> structureRoleList = getStructureRoleCodeList();
			if (structureRoleList.contains(nodeCode)) {
				String[] roles = ExamHelper.getPropertyValue(STRUCTURE_FILE, nodeCode + ROLE_SUFFIX).split(",");
				verticalNode.setRoleList(Arrays.asList(roles));
			}
			
			verticalMap.put(nodeCode, verticalNode);
		}

		// Check for parents
		defineVerticalParentNodes(verticalMap);

	}

	private void defineVerticalParentNodes(Map<String, VerticalNode> verticalNodeMap) {
		for (VerticalNode verticalNode : verticalNodeMap.values()) {
			if (verticalNode.getCode().contains("_")) {
				int lastParentIndex = verticalNode.getCode().lastIndexOf("_");
				String parentNodeCode = verticalNode.getCode().substring(0, lastParentIndex);

				VerticalNode parentVerticalNode = verticalNodeMap.get(parentNodeCode);
				if (parentVerticalNode == null) {
					throw new RuntimeException("Invalid vertical parent node: " + parentNodeCode);
				}

				verticalNode.setParentNode(parentVerticalNode);
			}
		}
	}

	private void loadHorizontalNodes() {
		String[] horizontals = ExamHelper.getPropertyValue(STRUCTURE_FILE, HORIZONTAL_NODES_PROPERTIES).split(",");

		horizontalMap = new HashMap<>();
		for (String nodeCode : horizontals) {
			HorizontalNode horizontalNode = new HorizontalNode();
			horizontalNode.setCode(nodeCode);
			horizontalNode.setTitle("Horizontal " + nodeCode);

			// Load roles
			List<String> structureRoleList = getStructureRoleCodeList();
			if (structureRoleList.contains(nodeCode)) {
				String[] roles = ExamHelper.getPropertyValue(STRUCTURE_FILE, nodeCode + ROLE_SUFFIX).split(",");
				horizontalNode.setRoleList(Arrays.asList(roles));
			}
			
			horizontalMap.put(nodeCode, horizontalNode);
		}

		// Check for parents
		defineHorizontalParentNodes(horizontalMap);
	}

	private void defineHorizontalParentNodes(Map<String, HorizontalNode> horizontalNodeMap) {
		for (HorizontalNode horizontalNode : horizontalNodeMap.values()) {
			if (horizontalNode.getCode().contains("_")) {
				int lastParentIndex = horizontalNode.getCode().lastIndexOf("_");
				String parentNodeCode = horizontalNode.getCode().substring(0, lastParentIndex);

				HorizontalNode parentHorizontalNode = horizontalNodeMap.get(parentNodeCode);
				if (parentHorizontalNode == null) {
					throw new RuntimeException("Invalid vertical parent node: " + parentNodeCode);
				}

				horizontalNode.setParentNode(parentHorizontalNode);
			}
		}
	}

	private void loadResponsibilityNodes() {
		responsibilityMap = new HashMap<>();

		String[] responsibilities = ExamHelper.getPropertyValue(STRUCTURE_FILE, RESPONSIBILITY_NODES_PROPERTIES)
				.split(",");
		for (String nodeStructure : responsibilities) {
			ResponsibilityNode node = new ResponsibilityNode();

			if (nodeStructure.contains("-")) {
				// Vertical comes first and horizontal comes second.
				// Child responsibility nodes don't have verticals or horizontals
				String[] stuctures = nodeStructure.split("-");
				if (stuctures.length != 3) {
					throw new RuntimeException("Invalid responsibility structure: " + nodeStructure);
				}

				VerticalNode verticalNode = verticalMap.get(stuctures[1]);
				HorizontalNode horizontalNode = horizontalMap.get(stuctures[2]);
				if (verticalNode == null) {
					throw new RuntimeException("Invalid vertical node: " + stuctures[1]);
				}
				if (horizontalNode == null) {
					throw new RuntimeException("Invalid horizontal node: " + stuctures[2]);
				}

				node.setHorizontalNode(horizontalNode);
				node.setVerticalNode(verticalNode);
				node.setCode(stuctures[0]);
				node.setTitle("Responsibility " + stuctures[0]);
			} else {
				// This node has a parent
				node.setCode(nodeStructure);
				node.setTitle("Responsibility " + nodeStructure);

				int lastParentIndex = nodeStructure.lastIndexOf("_");
				String parentNodeCode = nodeStructure.substring(0, lastParentIndex);

				ResponsibilityNode parentNode = responsibilityMap.get(parentNodeCode);
				if (parentNode == null) {
					throw new RuntimeException("Invalid responsibility parent node: " + parentNodeCode);
				}

				node.setParentNode(parentNode);
			}
			
			// Load roles
			List<String> structureRoleList = getStructureRoleCodeList();
			if (structureRoleList.contains(node.getCode())) {
				String[] roles = ExamHelper.getPropertyValue(STRUCTURE_FILE, node.getCode() + ROLE_SUFFIX).split(",");
				node.setRoleList(Arrays.asList(roles));
			}

			responsibilityMap.put(node.getCode(), node);
		}
		
	}

	/**
	 * Gets the list of vertical nodes
	 */
	public List<VerticalNode> getVerticalNodeList() {
		if (verticalMap == null) {
			loadVerticalNodes();
		}

		return new ArrayList<>(verticalMap.values());
	}

	/**
	 * Gets the list of horizontal nodes
	 */
	public List<HorizontalNode> getHorizontalNodeList() {
		if (horizontalMap == null) {
			loadHorizontalNodes();
		}

		return new ArrayList<>(horizontalMap.values());
	}

	/**
	 * Gets the list of responsibility nodes
	 */
	public List<ResponsibilityNode> getResponsibilityNodeList() {
		if (verticalMap == null) {
			loadVerticalNodes();
		}

		if (horizontalMap == null) {
			loadHorizontalNodes();
		}

		if (responsibilityMap == null) {
			loadResponsibilityNodes();
		}

		return new ArrayList<>(responsibilityMap.values());
	}

	public List<String> getStructureRoleCodeList() {
		if (structureRoleCodeList == null) {
			structureRoleCodeList = Arrays.asList(ExamHelper.getPropertyValue(STRUCTURE_FILE, STRUCTURES_WITH_ROLES).split(","));
		}
		return structureRoleCodeList;
	}
	
}
