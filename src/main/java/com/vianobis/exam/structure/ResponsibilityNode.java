package com.vianobis.exam.structure;

/**
 * Responsibility nodes can have the following configuration: 
 *   - A vertical and horizontal node; 
 *   - A parent responsibility node (vertical and horizontal are not allowed in this configuration).
 */
public class ResponsibilityNode extends AbstractStructureNode {

	private static final long serialVersionUID = 1L;

	private VerticalNode verticalNode;
	private HorizontalNode horizontalNode;

	public HorizontalNode getHorizontalNode() {
		return horizontalNode;
	}

	public void setHorizontalNode(HorizontalNode horizontalNode) {
		this.horizontalNode = horizontalNode;
	}

	public VerticalNode getVerticalNode() {
		return verticalNode;
	}

	public void setVerticalNode(VerticalNode verticalNode) {
		this.verticalNode = verticalNode;
	}

}
