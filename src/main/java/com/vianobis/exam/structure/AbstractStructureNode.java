package com.vianobis.exam.structure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractStructureNode implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Owner role, to be used as a role of structure
	 */
	public static final String OWNER_ROLE = "OWNER";
	/**
	 * Admin role, to be used as a role of structure
	 */
	public static final String ADMIN_ROLE = "ADMIN";
	/**
	 * Participant role, to be used as a role of structure
	 */
	public static final String PARTICIPANT_ROLE = "PARTICIPANT";

	private String code;
	private AbstractStructureNode parentNode;
	private String title;

	private List<String> roleList;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public AbstractStructureNode getParentNode() {
		return parentNode;
	}

	public void setParentNode(AbstractStructureNode parentNode) {
		this.parentNode = parentNode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getRoleList() {
		if (roleList == null) {
			roleList = new ArrayList<String>();
		}

		return roleList;
	}

	public void setRoleList(List<String> roleList) {
		this.roleList = roleList;
	}

	@Override
	public String toString() {
		return code + " - " + title;
	}
}
