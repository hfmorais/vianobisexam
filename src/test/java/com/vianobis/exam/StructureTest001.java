/**
 *
 */
package com.vianobis.exam;

import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.vianobis.exam.structure.AbstractStructureNode;
import com.vianobis.exam.structure.ResponsibilityNode;

class StructureTest001 {

	@Test
	void test() {
		StructureBuilder builder = new StructureBuilder();
		List<ResponsibilityNode> rNodeList = builder.getResponsibilityNodeList();

		ResponsibilityNode rNode = rNodeList.stream().filter(node -> node.getCode().equals("R1")).findFirst()
				.get();

		StructureTest test = new StructureTest();
		assertFalse(test.sctructureHasRole(rNode, AbstractStructureNode.ADMIN_ROLE));
	}

}
